export function convertLocalJson(content){
    let array = [];
    content.split("\n").map((item, key) => {
        item = item.replace(/\t| /g, ",");
        item = jsonConverter(item);
        array.push(item);
    })

    return getFormatedJson(array);
}

function getFormatedJson(data) {
    const formatted = data.reduce((acc, item) => {
        // Check if there are no items with this ID in accumulator still
        if (!acc.some(r => r.id === item.id)) {
            // Find all records with same ID
            const withSameId = data.filter(r => r.id === item.id);
            acc.push(withSameId[0]);

            if (withSameId.length > 1) {
                acc.push(withSameId[withSameId.length - 1]);
            }
        }
        return acc;
    }, []);
    formatted.pop();
    return formatted;
}

function jsonConverter(item){
        
        let date;
        let id;
        let time;
       item.split(',').map(function (value, key) {
            if (key === 0){
                id = value;
            }else if (key === 2 ){
                date = value;
            }else if (key === 3){
                time = value;
            }
        });
       
        let arr =({
            id: id,
            date: date,
            time: time
        });

      
        return arr;
    }