import React, { Component } from 'react';
import { Button, Col, Form, Row, Container, Alert} from 'react-bootstrap';
import { convertLocalJson} from './Datfileconverter';
import axios from 'axios';
import TableRow from './TableRow'




class Fileupload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            localjson: null,
            floor : null,
            isShowAlert : false,
            file:null,
            result : null
       
        }   
      
    }
    
    onChangeHandler = event => {
        
        this.setState({
            file: event.target.files[0]
        })
        

        var fileReader = new FileReader();
        fileReader.readAsText(event.target.files[0]);
        fileReader.onloadend = function (e) {
            var content = fileReader.result;

            this.setState({
                localjson: convertLocalJson(content)
            })

        }.bind(this);
       
    }    

    onClickHandler = event => {
        if (this.state.floor === 'Select Floor' || this.state.floor === null 
        || this.state.file === null){
            this.setState({
                isShowAlert: true
            })
        
        }else{
               
            this.checkthediff();
              
        }
      
        
        
    }


    
    onChangeSelect = e =>{
        this.setState({
            floor : e.target.value
        })
    }

    checkthediff(){
        let array = []; var i = 0;
    
       this.state.localjson.map((item) => {
           this.getEmplyeeWithParams(item,array,i);
        });
    }

    getEmplyeeWithParams(item, array,i){


        

       var floor = this.state.floor;
      
       var  date = this.formatDate(item.date);
       var  time = this.formatTime(item.time);
       var  id = item.id;
        item.date = this.formatDate(item.date);
        item.time = this.formatTime(item.time);
        axios.get(`http://localhost:3300/api/employees/${id}/${date}/${time}/${floor}`)
            .then(response => {
                if (response.data === false) {
                    array.push(item);
                    this.setState({
                        result: array
                    });
                    console.log(response);
                }
                if (id == "8080"){
                    console.log(response.data);
                    console.log(response.data.code);
                }
              
            },
                (error) => { console.log(error) }
            )
    }
    formatTime(value){
   
        return value.replace(/(\d+)$/gm,"00");
    }
    formatDate(value) {
        var date = new Date(value);    
        return (this.formatNumber(date.getMonth() + 1)) + '-' + this.formatNumber(date.getDate()) + '-' + date.getFullYear();
    }


    formatNumber(value) {
        return ('0' + value).slice(-2);
    }

    
  

 showAlert() {
    if (this.state.isShowAlert) {
        return (
            <Container>
                <Alert variant="danger" >
                    <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
                    <p>
                      Please Select A floor or add a file.
                    </p>
                </Alert>
            </Container>

        );
    }
}

    tabRow() {
        if (this.state.result != null) {
        return (
         
            this.state.result.map((item,key) => {
                return <TableRow obj={item} key ={key}/>;
            })
      
        );
        }
    }

    render() {
        return (
            <div>

                {this.showAlert()}
                 
                <Container>
                    <Row className="centered" >
                        <Col md={6}>
                            <Row>
                                <Col>

                                    <Form.Label>Upload Your File </Form.Label>
                                </Col>
                                <Col >
                                    <Form.Group controlId="exampleForm.ControlSelect1" 
                                        onChange={this.onChangeSelect}>
                                        <Form.Control as="select">
                                            <option>Select Floor</option>
                                            <option>RFC 5TH FLOOR</option>
                                            <option>RFC 7TH FLOOR</option>
                                            <option>RFC 8TH FLOOR</option>
                                            <option>RFC 11TH FLOOR</option>
                                        </Form.Control>
                                    </Form.Group>

                                </Col>
                            </Row>
                                   
                            <Row>
                                <Col>
                                <Form>
                                    <Form.Group className="files color">
                                    <Form.Control type="file" multiple="" name="file"
                                    onChange={this.onChangeHandler} />
                                    </Form.Group>
                                    <Button size="lg" block variant="success"
                                    onClick={this.onClickHandler}>Upload</Button>
                                </Form>
                                </Col>
                             </Row>

                            
                        </Col>
                    </Row>

                    <table className="table table-striped" style={{ marginTop: 20 }}>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.tabRow()}
                        </tbody>
                    </table>
                </Container>


            </div>
        );
    }
}

export default Fileupload;
