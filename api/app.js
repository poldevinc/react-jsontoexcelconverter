var express = require('express');
var bodyParser = require('body-parser');
const cors = require('cors');
var router = require('./routes')();

var app = express();
var port = process.env.port || 3300


app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api', router);


app.listen(port, () => {
    console.log("Hi This port is running");
});
