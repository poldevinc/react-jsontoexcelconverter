const _employeeRepository = require('./employee.respository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const employeeRepository = _employeeRepository(dbContext);

    router.route('/employees/:id/:date/:time/:floor')
        .get(employeeRepository.getAll);



    router.route('/employees/')
        .get(employeeRepository.getAlltest);

}
