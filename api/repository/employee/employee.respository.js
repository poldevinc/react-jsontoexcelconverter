
var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;

function EmployeeRepository(dbContext) {

    function getEmployees(req, res) {
        var id = req.params.id;
        var date = req.params.date;
        var time = req.params.time;
        var floor = req.params.floor;

        console.log(floor);
     //  console.log(stattda);
        var parameters = [];
        var query = `select ID, 
                convert(varchar, DateTrans,110) as [Date],
                convert(varchar, DateTrans,108) as [Time] from TimeInOut 
                where 
                location = '${floor}' and 
                ID = '${id}'and
                convert(varchar, DateTrans,110) = '${date}' and 
                convert(varchar, DateTrans,108) = '${time}'`;
        console.log(query);
        dbContext.getQuery(query, parameters, false, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function getEmployeesTest(req, res) {
        var parameters = [];
        var query = `select ID,
convert(varchar, DateTrans,110) as [Date],
convert(varchar, DateTrans,108) as [Time] from TimeInOut
where
location = 'RFC 5TH FLOOR' and
convert(varchar, DateTrans,110) between  '07-30-2019' and '08-30-2019'`;

        dbContext.getQuery(query, parameters, false, function (error, data) {
            return res.json(response(data, error));
        });
    }


    
    return {
        getAll: getEmployees,
        getAlltest: getEmployeesTest
    }
}

module.exports = EmployeeRepository;