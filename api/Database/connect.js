var Connection = require('tedious').Connection; 
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');

var server = cryptr.decrypt("b6975fbd0a9f6a2ad51fc0b3c34cab992989d8f6727c837f9c69d356c319c1c644e372f1c035c66187eaf922f572898246d3b3ae1fad45fdf73aa3c14f2a81e4390e");
var username = cryptr.decrypt("4f15b2f46e00780ce50f99a6b1b63bdfd2a8c57c1a59ef7e");
var password = cryptr.decrypt("fa2bc796f734b0531db5b2d6fff467d87bbfb7710c554117");
var database = cryptr.decrypt("84b49d732c83f72afcedf3d8c2a364455524564278049b");
var config = {
    server: server,
    authentication: {
        type: 'default',
        options: {
            userName: username,
            password: password
        }
    },
    options: {
        database: database,
        rowCollectionOnDone: true,
        useColumnNames: false
    }
}

var connection = new Connection(config);

connection.on('connect', function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('Connected');
    }
});

module.exports = connection;
